package ru.t1.godyna.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.service.IAuthService;
import ru.t1.godyna.tm.api.service.IUserService;
import ru.t1.godyna.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {


    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    public IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

}
