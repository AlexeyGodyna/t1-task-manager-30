package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.TaskSort;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-list";

    @NotNull
    private final String DESCRIPTION = "Show list tasks.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final List<Task> tasks = getTaskService().findAll(getUserId(), sort.getComparator());
        @NotNull final StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (@Nullable final Task task: tasks) {
            if (task == null) continue;
            stringBuilder.append(index + ". ");
            stringBuilder.append(task.getName() + " : ");
            stringBuilder.append(task.getDescription() + " : ");
            stringBuilder.append(task.getId() + " : ");
            stringBuilder.append(task.getProjectId());
            System.out.println(stringBuilder);
            index++;
            stringBuilder.setLength(0);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
