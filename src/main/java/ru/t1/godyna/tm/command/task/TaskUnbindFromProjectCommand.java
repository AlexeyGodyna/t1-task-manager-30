package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-unbind-from-project";

    @NotNull
    private final String DESCRIPTION = "Unbind task from project.";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(getUserId(), projectId, taskId);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
