package ru.t1.godyna.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported...");
    }

    public CommandNotSupportedException(@NotNull final String cmdName) {
        super("Error! Command '" + cmdName + "' is not supported...");
    }

}
